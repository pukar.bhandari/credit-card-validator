// Credit Card Validator:
// • Number must be 16 digits, all of them must be numbers
// • You must have at least two different digits represented (all of the digits cannot be the same)
// • The final digit must be even
// • The sum of all the digits must be greater than 16
// A valid credit card number may also contain dashes, to make a card number easier to read.
//
//     The Frontend should contain an input box to enter a credit card number containing dash.
//     Should display a message if the entered number is valid or not.
const cardno = document.querySelector('.cardno');
const btn = document.querySelector('button');
const valid = document.querySelector(".validation");

function createParagraph() {
    const num = cardno.value;
    let para = document.createElement('p');
    para.style.textAlign='center';
    para.style.fontSize='24px';
    para.style.color='white';
    let check = num.split("");
    valid.textContent = '';
    for (let i = 0; i < check.length; i++) {
        if (check[i] === '-' || check[i] === ' ') {
            check.splice(i, 1);
        }
    }
    if (num === '' || num === null) {
        para.textContent = "No card number provided";
        para.style.backgroundColor='red';
        return valid.appendChild(para);
    }
    // if (check.filter((i) => isNaN(i)).length !== 0 || check.length !== 16 || allEqual(check)===true ||
    //     check.map((i) => Number(i)).reduce((total, n) => total + n, 0) <= 16
    //     || Number(check[check.length - 1]) % 2 !== 0 ){
    //     para.textContent = 'Invalid';
    //     para.style.backgroundColor='red';
    //     return valid.appendChild(para);
    // }
    if (check.filter((i) => isNaN(i)).length !== 0){
        para.textContent = 'Invalid (Non-numeric characters not allowed)';
        para.style.backgroundColor='red';
        return valid.appendChild(para);
    }
    if (check.length !== 16){
        para.textContent = 'Invalid (Card number must be of 16 digits)';
        para.style.backgroundColor='red';
        return valid.appendChild(para);
    }
    const allEqual= arr => arr.every( v => v === arr[0] );
    if (allEqual(check)===true){
        para.textContent = 'Invalid (At least two digits must be different)';
        para.style.backgroundColor='red';
        return valid.appendChild(para);
    }
    if (check.map((i) => Number(i)).reduce((total, n) => total + n, 0) <= 16){
        para.textContent = 'Invalid (Sum of all digits must be greater than 16)';
        para.style.backgroundColor='red';
        return valid.appendChild(para);
    }
    if (Number(check[check.length - 1]) % 2 !== 0 ){
        para.textContent = 'Invalid (Last digit must be even)';
        para.style.backgroundColor='red';
        return valid.appendChild(para);
    }
    para.textContent = 'Valid';
    para.style.backgroundColor='green';
    return valid.appendChild(para);
}
cardno.focus();
btn.addEventListener('click', createParagraph);