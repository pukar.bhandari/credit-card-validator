<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Credit card validator</title>
    <link href="../css/style.css" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
</head>
<body>
<div class="credit">
    <div class="wrapper">
        <div class="header">
            <h1>Online credit card validator</h1>
            <p>This tool validates if a credit card is valid or not. You can enter the card number either in 4 digit
                groups, with spaces or without any spaces.</p>
        </div>

        <div class="input">
            <label for="cardno">Credit Card Number:</label>
            <input type="tel" id="cardno" class="cardno"/>
            <button type="submit">Validate credit card number</button>
        </div>

        <div class="validation"></div>
    </div>
</div>
<script src="../js/index.js"></script>
</body>
</html>